module.exports = {
  mail: require("./Factory/FactoryMail"),
  redis: require("./Factory/FactoryRedis"),
  route: require("./Factory/FactoryRoute"),
  request: require("./Factory/FactoryRequest"),
  schema: require("./Factory/FactorySchema"),
  sequelize: require("./Factory/FactorySequelize"),
  stripe: require("./Factory/FactoryStripe"),
  utils: require("./Factory/FactoryUtils"),
  token: require("./Factory/FactoryToken"),
  logger: require("./Factory/FactoryLogger")
}