const { promisify } = require('util');
const r = require("request");
const { btoa } = require("./FactoryUtils");

const request = r.defaults({
  baseUrl: process.env.CONNECTOR_URL,
  json: true,
  headers: { "Vivlab-Connector-Key": btoa(process.env.CONNECTOR_KEY) }
});

const promise = method => {
  request[method][promisify.custom] = options => {
    return new Promise((resolve, reject) => {
      request[method](options, (err, response, body) => {
        if (err) reject(err);
        else resolve(body);
      })
    });
  };
  return promisify(request[method]);
};

const get = promise("get");
const post = promise("post");

module.exports = { get, post };