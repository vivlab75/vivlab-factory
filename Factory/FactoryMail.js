const nodemailer = require('nodemailer'),
  transporter = nodemailer.createTransport({
    host: 'mail.vivlab.com',
    port: 587,
    secure: false, // true for 465, false for other ports
    tls: {
      rejectUnauthorized: false
    },
    auth: {
      user: process.env.MAIL_USER,
      pass: process.env.MAIL_PASSWORD
    }
  });
const fs = require('fs');

require.extensions['.html'] = function (module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8');
};
const model = require("../Template/MailModel.html");
const interfaceUrl = process.env.INTERFACE_URL;

const generate = (content, link, button) => {
  let m = model.replace(/\${mail-content}/, content);
  m = m.replace(/\${mail-link}/, link || interfaceUrl);
  m = m.replace(/\${mail-button}/, button || "Accéder à l'interface");
  return m
}

module.exports = {
  send(to, { content, link = undefined, button = undefined }, subject) {
    return new Promise((resolve, reject) => {
      let option = { from: '"Vivlab SAS" <support@vivlab.com>', to, html: generate(content, link, button), subject };
      transporter.sendMail(option, (error, info) => {
        if (error) reject(error)
        else resolve(info)
      });
    })
  }
}