const { promisify } = require('util');
const redis = require("redis");
const client = redis.createClient({
  host: process.env.REDIS_HOST,
  retry_strategy: options => 6000
});
const promise = (command) => promisify(client[command]).bind(client);
const SCAN = promise("scan");
const GET = promise("get");
const MGET = promise("mget");
const HGET = promise("hget");
const HMGET = promise("hmget");
const SET = promise("set");
const MSET = promise("mset");
const HSET = promise("hset");
const HMSET = promise("hmset");
const DEL = promise("del");
const HDEL = promise("hdel");
const FLUSHALL = promise("flushall");
const ZADD = promise("zadd");
const ZRANGE = promise("zrange");
const ZREVRANGE = promise("zrevrange");
const ZRANGEBYSCORE = promise("zrangebyscore");
const ZREVRANGEBYSCORE = promise("zrevrangebyscore");
const ZREMRANGEBYSCORE = promise("zremrangebyscore");


module.exports = {
  SCAN,
  GET,
  MGET,
  HGET,
  HMGET,
  SET,
  MSET,
  HSET,
  HMSET,
  DEL,
  HDEL,
  FLUSHALL,
  ZADD,
  ZRANGE,
  ZREVRANGE,
  ZRANGEBYSCORE,
  ZREVRANGEBYSCORE,
  ZREMRANGEBYSCORE
};