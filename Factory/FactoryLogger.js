const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf, simple, json } = format;
const os = require('os');
const hostname = os.hostname();

const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${hostname}] [${label}] ${level}: ${JSON.stringify(message)}`;
});

const url = new URL(process.env.CONNECTOR_URL);

const logger = createLogger({
  level: 'info',
  format: combine(
    label({ label: (process.env.PWD || process.cwd()).match(/[^\/]*$/)[0] }),
    timestamp(),
    myFormat
  ),
  transports: [
    new transports.Http({
      host: url.hostname,
      port: Number(url.port) || undefined,
      path: "/loggers",
      ssl: url.protocol === "https:" ? true : false
    })]
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new transports.Console({
    format: simple()
  }));
}

module.exports = { logger, transports };