const { atob } = require("./FactoryUtils");
const jwt = require('jsonwebtoken'),
  token = {
    access: {
      private: atob(process.env.ACCESS_PRIVATE),
      public: atob(process.env.ACCESS_PUBLIC),
      alg: "RS256",
      expiresIn: process.env.TOKEN_ACCESSEXPIRES
    },
    refresh: {
      private: atob(process.env.REFRESH_PRIVATE),
      public: atob(process.env.REFRESH_PUBLIC),
      alg: "RS256",
      expiresIn: process.env.TOKEN_REFRESHEXPIRES
    }
  };

module.exports = {
  create(payload, tokenType) {
    return new Promise(resolve => {
      let list = tokenType == "all" ? ["access", "refresh"] : [tokenType];
      let tokenList = list.reduce((a, v) => {
        let data = { ...payload, type: v };
        let option = { algorithm: token[v].alg, expiresIn: token[v].expiresIn };
        return { ...a, [v]: jwt.sign(data, token[v].private, option) }
      }, {});
      resolve(tokenList)
    })
  },
  verify(verify, tokenType) {
    return new Promise((resolve, reject) => {
      jwt.verify(verify, token[tokenType].public, { algorithm: token[tokenType].alg }, (err, data) => {
        if (err) reject("invalid token")
        else {
          let { email, acid, status, description } = data;
          resolve({ email, acid, status, description })
        }
      })
    })
  },
  parseHeader(auth) {
    let token = auth && auth.match(/^Bearer\s(.+)/i);
    if (token) return token[1]
    else throw new Error("invalid token")
  }
}