const stripe = require('stripe')(process.env.STRIPE_SECRET);
stripe.setApiVersion(process.env.STRIPE_VERSION);
const endpointSecret = process.env.STRIPE_ENDPOINT;

const stripeList = async () => {
  const arr = [];
  let starting_after;
  let hasMore = true;
  while (hasMore) {
    let { has_more, data } = await stripe.customers.list({ starting_after, limit: 100 });
    starting_after = data[data.length - 1].id;
    hasMore = has_more;
    arr.push(...data);
  }
  return arr;
}

const stripeOffer = async () => {
  const [{ data: products }, { data: plans }] = await Promise.all([stripe.products.list(), stripe.plans.list()]);
  return [products, plans];
}

module.exports = { stripe, endpointSecret, stripeList, stripeOffer };