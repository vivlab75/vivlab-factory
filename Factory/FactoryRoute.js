module.exports = fn => (req, res, next) => {
  Promise.resolve(fn(req, res, res.locals.acid)).catch(next);
};
