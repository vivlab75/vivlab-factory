const generate = require('nanoid/generate'),
  alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
module.exports = {
  generateId() {
    return generate(alphabet, 10);
  },
  atob(data) {
    if (data === undefined) return undefined;
    return Buffer.from(data, 'base64').toString('utf-8');
  },
  btoa(data) {
    if (data === undefined) return undefined;
    return Buffer.from(data).toString('base64');
  }
}