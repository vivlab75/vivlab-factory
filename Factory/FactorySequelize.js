const Sequelize = require('sequelize');
const sequelize = new Sequelize({
  database: process.env.MYSQL_DATABASE,
  username: process.env.DB_USER,
  password: process.env.MYSQL_ROOT_PASSWORD,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  dialect: 'mariadb',
  logging: false,
  dialectOptions: {
    timezone: 'Etc/GMT+2',
  },
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

module.exports = { sequelize, Sequelize }