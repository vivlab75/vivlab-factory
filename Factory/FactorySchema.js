const Ajv = require('ajv'),
  ajv = new Ajv({ allErrors: true, removeAdditional: "all", useDefaults: false, jsonPointers: true });
require('ajv-errors')(ajv /*, {singleError: true} */);

module.exports = async (schema, data, required) => {
  let validate = ajv.compile({ ...schema, $async: true, required });
  try {
    return await validate(data);
  }
  catch (e) {
    throw new Error(e.errors[0].message || e.message || e);
  }
};